Feature: Make a post call to upload a image 

Scenario: To make a post call to check image upload successful 
  Given POST api endpoint for batch upload 
  When Set HEADER param request content type as "application/json"
  And Set Body param file as "image" 
  And Set Body param type as "markedup" or "preprocessed" or "touchedup"
  And Raise "POST" HTTP request
  Then Perform basic sanity check for the response
  And JSON at path ".data.message" should equal "success"