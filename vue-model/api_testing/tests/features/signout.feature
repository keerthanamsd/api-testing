Feature: Check the Signout functionality

Background:
	Given Set basic web application url

Scenario: Check Signout
  Given Set POST api endpoint for signout
  When Raise "POST" HTTP request
  Then Perform basic sanity check for the response
  Then JSON at path ".data.message" should equal "Successfully logged out"
