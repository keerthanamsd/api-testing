Feature: Check product selected models

Scenario: Check selected models
  Given Set POST api endpoint for product select models
  When Set HEADER param request content type as "application/json"
  When Set HEADER param response accept type as "application/json"
  When Set BODY param as "batchname" , "product_id"
  When Raise "POST" HTTP request
  Then Perform basic sanity check for the response
  And Check JSON at path ".data.data.front" should not be null 
  And check JSON at path ".data.data.back" should not be null
