Feature: Check the Signup functionality

Background:
	Given Set basic web application url

Scenario: Check Signup
  Given Set POST api endpoint for signup 
  When Set HEADER param request content type as "application/json"
  And Set HEADER param response accept type as "application/json"
  And Set BODY param request with "username" 
  And Set BODY param request with "email" 
  And Set BODY param request with "password" 
  And Set BODY param request with "role" 
  And Set Query param as "empty"
  And Raise "POST" HTTP request
  Then Perform basic sanity check for the response
  And JSON at path ".data.message" shoule equal "Successfully created user"
