Feature: Check the generate functionality

Scenario: generate a product 
  Given To make POST api call with to generate model 
  When Set HEADER param request content type as "application/json"
  When Set HEADER param response accept type as "application/json"
  When BODY has required params with "modelname" , "model_type" and "net_version"
  When Raise "POST" HTTP request
  Then Perform basic sanity check for the response
  And JSON at path ".data.message" should equal "generated"
