Feature: Check the Signin functionality

Background:
	Given Set basic web application url

Scenario: Check Signin
  Given Set POST api endpoint as "user/signin"
  When Set HEADER param request content type as "application/json"
  When Set HEADER param response accept type as "application/json"
  When Set BODY param request with "username" as "client1"
  When Set BODY param request with "password" as "1234"
  When Set Query param as "empty"
  When Raise "POST" HTTP request
  Then Perform basic sanity check for the response
