Feature: Retrieve batch details using REST API and analyze the results

Background:
	Given Set basic web application url
	Given Set the session id with credentials username as "client1" and password as "1234"

Scenario: Get batch details
  Given Set GET api endpoint as "batch/9cc70f2f-12cb-4be7-bb06-0bb151c4d80c"
  When Set HEADER param request content type as "application/json"
	And Set Query param as "empty"
	And Raise "GET" HTTP request
	Then Valid HTTP response should be received
	And Response http code should be 200
	And Response HEADER content type should be "application/json"
	And Response BODY should not be null or empty
