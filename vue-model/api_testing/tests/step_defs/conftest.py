#!/usr/bin/python
# -*- coding: utf-8 -*-

from pytest_bdd import given, when, then, parsers
import requests
import yaml
import json
import polling

global_general_variables = {}
http_request_header = {}
http_request_body = {}
http_request_url_query_param = {}
global_session_id = None
cookies = {}


@given(u'Set basic web application url')
def basic_web_app():
    stream = open('step_defs/config.yml', 'r')
    config = yaml.load(stream, Loader=yaml.BaseLoader)
    environment = config.get("environment")
    url = config.get("urls").get("login").get("url")
    url = f"{environment}{url}"
    global_general_variables['basic_application_URL'] = url
    http_request_header['content-type'] = config.get("urls").get("login").get("header").get("content_type")

@given(parsers.parse(u'Set the session id with credentials username as "{username}" and password as "{password}"'))
def set_session_id(request,username,password):
    global_general_variables['GET_api_endpoint'] = '/user/signin'
    http_request_body["username"] = username
    http_request_body["password"] = password
    url_temp = global_general_variables['basic_application_URL'] + global_general_variables['GET_api_endpoint']
    global_general_variables['response_full'] = requests.post(url_temp, headers=http_request_header, params=http_request_url_query_param, data=json.dumps(http_request_body))
    global_general_variables['sessionid'] = global_general_variables['response_full'].cookies['sessionid']
    cookies['sessionid'] = global_general_variables['sessionid']
    global_session_id = global_general_variables['sessionid']
    request.config.cache.get('session_id', None)
    request.config.cache.set('session_id', global_session_id)
    http_request_body.clear()

@when(parsers.parse(u'Set HEADER param request content type as "{header_content_type}"'))
def set_content_type(header_content_type):
    http_request_header['content-type'] = header_content_type

@when(parsers.parse(u'Set HEADER param response accept type as "{header_accept_type}"'))
def set_header_accept_type(header_accept_type):
    http_request_header['Accept'] = header_accept_type

@when(parsers.parse(u'Set BODY param request with "{key}" as "{value}"'))
def set_header_accept_type(key,value):
    http_request_body[key] = value

@given(parsers.parse(u'Set GET api endpoint as "{get_api_endpoint}"'))
def set_get_api_endpoint(get_api_endpoint):
    global_general_variables['GET_api_endpoint'] = get_api_endpoint


@given(parsers.parse(u'Set POST api endpoint as "{post_api_endpoint}"'))
def set_post_api_endpoint(post_api_endpoint):
    global_general_variables['POST_api_endpoint'] = post_api_endpoint


@when(parsers.parse(u'Set PUT api endpoint as "{put_api_endpoint}"'))
def set_put_api_endpoint(put_api_endpoint):
    global_general_variables['PUT_api_endpoint'] = put_api_endpoint


@when(parsers.parse(u'Set DELETE api endpoint as "{delete_api_endpoint}"'))
def set_del_api_endpoint(delete_api_endpoint):
    global_general_variables['DELETE_api_endpoint'] = delete_api_endpoint


@when(parsers.parse(u'Set Query param as "{query_param}"'))
def set_query_params(query_param):
    if 'empty' in query_param:
        http_request_url_query_param.clear()
    else:
        http_request_url_query_param.clear()
        http_request_url_query_param['signout_emailid'] = global_general_variables['email']
        http_request_url_query_param['session_id'] = global_general_variables['latest_session_key']

def is_correct_response(response):
    """Check that the response returned 'success'"""
    if not None in response:
        global_general_variables['response_full'] = response
        return True


@when(parsers.parse(u'Raise "{http_request_type}" HTTP request'))
def raise_request(request, http_request_type):
    url_temp = global_general_variables['basic_application_URL']
    if 'GET' == http_request_type:
        url_temp += global_general_variables['GET_api_endpoint']
        polling.poll(
            lambda: requests.get(url_temp,
                                                headers=http_request_header,
                                                params=http_request_url_query_param,
                                                data=http_request_body,
                                                cookies=cookies),
                                 check_success=is_correct_response,
                                 step=1,
                                 timeout=10)

        http_request_body.clear()
        request.config.cache.get('response_full', None)
        request.config.cache.set('response_full', global_general_variables['response_full'].json())

    elif 'POST' == http_request_type:
        url_temp += global_general_variables['POST_api_endpoint']
        http_request_url_query_param.clear()
        polling.poll(
            lambda: requests.post(url_temp,
                                 headers=http_request_header,
                                 params=http_request_url_query_param,
                                 data=json.dumps(http_request_body),
                                 cookies=cookies),
            check_success=is_correct_response,
            step=1,
            timeout=10)

        http_request_body.clear()
        request.config.cache.get('response_full', None)
        print(global_general_variables['response_full'].json())
        print(global_general_variables['response_full'].cookies['sessionid'])
        request.config.cache.set('response_full', global_general_variables['response_full'].json())
        request.config.cache.set('sessionid', global_general_variables['response_full'].cookies['sessionid'])

    elif 'PUT' == http_request_type:
        url_temp += global_general_variables['PUT_api_endpoint']
        http_request_url_query_param.clear()
        polling.poll(
            lambda: requests.put(url_temp,
                                 headers=http_request_header,
                                 params=http_request_url_query_param,
                                 data=http_request_body,
                                 cookies=cookies),
            check_success=is_correct_response,
            step=1,
            timeout=10)

        http_request_body.clear()
        request.config.cache.get('response_full', None)
        request.config.cache.set('response_full', global_general_variables['response_full'].json())
        request.config.cache.set('sessionid', global_general_variables['response_full'].cookies['sessionid'])

    elif 'DELETE' == http_request_type:
        url_temp += global_general_variables['DELETE_api_endpoint']
        http_request_body.clear()
        polling.poll(
            lambda: requests.delete(url_temp,
                                 headers=http_request_header,
                                 params=http_request_url_query_param,
                                 data=http_request_body,
                                 cookies=cookies),
            check_success=is_correct_response,
            step=1,
            timeout=10)

        http_request_body.clear()
        request.config.cache.get('response_full', None)
        request.config.cache.set('response_full', global_general_variables['response_full'].json())
        request.config.cache.set('sessionid', global_general_variables['response_full'].cookies['sessionid'])

@then(u'Valid HTTP response should be received')
def assert_nll_response():
    if None in global_general_variables['response_full']:
        assert False, 'Null response received'

@then(parsers.cfparse(u'Response http code should be {expected_response_code:d}'))
def validate_response_code(expected_response_code):
    global_general_variables['expected_response_code'] = expected_response_code
    actual_response_code = global_general_variables['response_full'].status_code
    if str(actual_response_code) not in str(expected_response_code):
        print (str(global_general_variables['response_full'].json()))
        assert False, '***ERROR: Following unexpected error response code received: ' + str(actual_response_code)


@then(parsers.parse(u'Response HEADER content type should be "{expected_response_content_type}"'))
def validate_response_content_type(expected_response_content_type):
    global_general_variables['expected_response_content_type'] = expected_response_content_type
    actual_response_content_type = global_general_variables['response_full'].headers['Content-Type']
    if expected_response_content_type not in actual_response_content_type:
        assert False, '***ERROR: Following unexpected error response content type received: ' + actual_response_content_type

@then(parsers.parse(u'Response BODY should not be null or empty'))
def validate_response_body_notnull():
    if None in global_general_variables['response_full']:
        assert False, '***ERROR:  Null or none response body received'

@then(parsers.parse(u'Perform basic sanity check for the response'))
def validate_response_body_notnull():
    if None in global_general_variables['response_full']:
        assert False, '***ERROR:  Null or none response body received'
    global_general_variables['expected_response_code'] = 200
    actual_response_code = global_general_variables['response_full'].status_code

    if str(actual_response_code) not in str(200):
        print(str(global_general_variables['response_full'].json()))
        assert False, '***ERROR: Following unexpected error response code received: ' + str(actual_response_code)

    global_general_variables['expected_response_content_type'] = "application/json"
    actual_response_content_type = global_general_variables['response_full'].headers['Content-Type']
    if "application/json" not in actual_response_content_type:
        assert False, '***ERROR: Following unexpected error response content type received: ' + actual_response_content_type
