from pytest_bdd import scenario, given, parsers

@scenario('../features/signin.feature', 'Check Signin')
def test_the_session_id(request):
    print("Successfully Logged in with below sessionid\n")
    global_session_id = request.config.cache.get('sessionid', None)
    print(global_session_id)