from behave import *
import requests
import json
import yaml

#---------------
# Background
#---------------
@given('batch_list,i have auth creds')
def set_impl(context):
    stream = open("config.yml", 'r')
    config = yaml.load(stream, Loader=yaml.BaseLoader)
    environment = config.get("environment")
    url =  config.get("urls").get("login").get("url")
    print(config)
    context.url = f"{environment}{url}/user/signin"
    context.headers = config.get("urls").get("login").get("header")
    context.body = {
        "username": "client1",
        "password": "1234",
    }

@when('batch_list,i attempt to login')
def step_impl(context):
    context.res = requests.post(context.url, data=json.dumps(context.body), headers=context.headers)
    print("res\n",context.res)
    context.cookies['sessionid'] = context.res.cookies['sessionid']

@then('batch_list,i must get a sessionid')
def step_impl(context):
    assert context.res.status_code == 200
    assert context.res.json().get('status') == 'success' 




@given('batch listing page')
def set_impl(context):
    stream = open("config.yml", 'r')
    config = yaml.load(stream, Loader=yaml.BaseLoader)
    environment = config.get("environment")
    url =  config.get("urls").get("login").get("url")
    context.url = f"{environment}{url}/batch"


@when('batch listing is success')
def step_impl(context):
    print("as",context.url)
    context.res = requests.get(context.url,headers=context.cookies)
    print("res\n",context.res.json())
    assert context.res.status_code == 200
        

@then('status code 200')
def step_impl(context):
    assert context.res.status_code == 200
    assert context.res.json().get('status') == 'success'
