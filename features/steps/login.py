from behave import *
import requests
import json
import yaml

@given('I have user authentication credentials')
def set_impl(context):

    stream = open("config.yml", 'r')
    config = yaml.load(stream, Loader=yaml.BaseLoader)
    environment = config.get("environment")
    url =  config.get("urls").get("login").get("url")
    print(config)
    context.url = f"{environment}{url}/user/signin"
    context.headers = config.get("urls").get("login").get("header")
    context.body = {
        "username": "client1",
        "password": "1234",
    }


@when('I make an http post call')
def step_impl(context):
    context.res = requests.post(context.url, data=json.dumps(context.body), headers=context.headers)
    context.cookies['sessionid'] = context.res.cookies['sessionid']
    print("cookies",context.cookies)
    print(context.res.json())

  

@then('I must get a reponse with status code 200')
def step_impl(context):
    assert context.res.status_code == 200
    assert context.res.json().get('status') == 'success' 
