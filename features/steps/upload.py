from behave import *
import requests
import json
import yaml

#---------------
# Background
#---------------
@given('i have auth creds')
def set_impl(context):
    stream = open("config.yml", 'r')
    config = yaml.load(stream, Loader=yaml.BaseLoader)
    environment = config.get("environment")
    url =  config.get("urls").get("login").get("url")
    print(config)
    context.url = f"{environment}{url}/user/signin"
    context.headers = config.get("urls").get("login").get("header")
    context.body = {
        "username": "client1",
        "password": "1234",
    }

@when('i attempt to login')
def step_impl(context):
    context.res = requests.post(context.url, data=json.dumps(context.body), headers=context.headers)
    print(context.res.json())

@then('i must get a sessionid')
def step_impl(context):
    assert context.res.status_code == 200
    assert context.res.json().get('status') == 'success' 
    context.cookies['sessionid'] = context.res.cookies['sessionid']
    print("cookies",context.cookies)

#---------------
# Feature
#---------------
@given('a batch-csv to upload')
def set_impl(context):
    context.url = 'https://staging-vuemodel.madstreetden.com/api/v0.1/batch'
    context.headers = {'content-type': 'multipart/form-data'}
    context.file = {'file': open('file1.csv','rb')} 

@when('make a post call to upload')
def step_impl(context):   
    headers = {"sessionid": context.cookies['sessionid']}
    print(headers)
    context.res = requests.post(context.url,files = context.file,headers=headers)
    assert context.res.status_code == 200
    print(context.res.json())
    batch_id = context.res.json().get('batch_id')
    print(batch_id)

@then('200 successful')
def step_impl(context):
    assert context.res.status_code == 200
    assert context.res.json().get('status') == 'success'
